# frozen_string_literal: true

module DeviseTokenAuth
  VERSION = '1.1.26'.freeze
end
